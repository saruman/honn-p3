var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var VideoSchema   = new Schema({
    name: {type: String, required: true},
    channel: {type: String, default: null}
});

module.exports = mongoose.model('Video', VideoSchema);