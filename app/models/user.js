var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    
    // Account service
    name: {type: String, required: true, unique: true},
    name_lower: {type: String, required: true, unique: true},
    password: {type: String, required: true},

    // User service
    favoriteVideos: {type: [String], default: null},
    closeFriends: {type: [String], default: null}
});

module.exports = mongoose.model('User', UserSchema);