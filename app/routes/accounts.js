var express    = require('express');

var User       = require('../models/user');
var router = express.Router({mergeParams: true});


router.route('/')


	.post(function(req, res) {
		
		var user = new User();      

		user.name = req.body.name;
		user.name_lower = user.name.toLowerCase();
		user.password = req.body.password;

		user.save(function(err) {
			if (err) {
				res.statusCode = 400;
				res.send(err);
			}
			else {
				res.json({
					message: 'Account ' + req.body.name + 
							 ' created with password ' + req.body.password + 
							 '!'
				});
			}
		});
		
	})

	.get(function(req, res) {
		User.find(function(err, users) {
			if (err)
				res.send(err);

			res.json(users);
		});
	});


router.route('/updatepassword')

	.post(function(req, res) {
		User.findOneAndUpdate({
			name_lower: req.body.name.toLowerCase(),
			password: req.body.oldPassword
		},
		{
			password: req.body.newPassword
		},
		{},
		function(err, doc) {
			if(err) {
				res.sendStatus(500);
			}
			res.sendStatus(200);
		});
	});


router.route('/auth')

	.post(function(req, res) {
		User.findOne({
			name_lower: req.body.name.toLowerCase(), 
			password: req.body.password
		})
		.then(function(user) {
			if(!user) {
				res.sendStatus(401);
			}
			else {
				req.session['username'] = user.name;
				res.sendStatus(200);
			}
		}, function(err) {
			res.sendStatus(500);
		});

	});

router.route('/logout')

	.get(function(req, res) {
		req.session = null;
		console.log("HERE");
		res.sendStatus(200);
	});

router.route('/:username')

	.get(function(req, res) {
		User.findOne({
			name_lower: req.params.username.toLowerCase()
		}, function(err, user, result) {
			if(err) {
				res.sendStatus(500);
			}
			res.json(user);
		});
	})

	// delete the account with this username
    .delete(function(req, res) {
        User.findOneAndRemove({
            name_lower: req.params.username.toLowerCase()
        }, function(err, user, result) {
            if (err){
                res.sendStatus(500);
            }
            res.sendStatus(204);
        });
    });

module.exports = router;