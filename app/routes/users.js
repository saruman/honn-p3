var express    = require('express');

var User       = require('../models/user');
var router = express.Router({mergeParams: true});

// more routes for our API will happen here

// on routes that end in /users
// ------------------------------------------------------------------------------
router.route('/')

	.get(function(req, res) {
		User.find(function(err, users) {
			if (err)
				res.send(err);

			res.json(users);
		});
	});


router.route('/current')
	.get(function(req, res) {
		User.findOne({name: req.session['username']}, function(err, user) {
			if (err)
				res.send(err);

			res.json(user);
		});
	})
	.put(function(req, res) {
		User.findOneAndUpdate(
			{name: req.session['username']}, 
			req.body,
			{new: true},
			function(err, doc) {
				if(err) {
					res.sendStatus(500);
				}
				res.send(doc);
		});
	});


router.route('/closeFriend')

	.post(function(req, res) {
		User.findOneAndUpdate({
			name: req.session['username'],
		},
		{
			$push: {closeFriends: req.body.name}
		},
		// new: true makes us return the updated document instead of the original.
		{new: true},
		function(err, doc) {
			if(err) {
				res.sendStatus(500);
			}
			res.send(doc);
		});
	});


router.route('/favoriteVideo')

	.post(function(req, res) {
		User.findOneAndUpdate({
			name: req.session['username'],
		},
		{
			$push: {favoriteVideos: req.body.name}
		},
		// new: true makes us return the updated document instead of the original.
		{new: true},
		function(err, doc) {
			if(err) {
				res.sendStatus(500);
			}
			res.send(doc);
		});
	});

module.exports = router;