var express    = require('express');

var Video       = require('../models/video');
var router = express.Router({mergeParams: true});

// more routes for our API will happen here

// on routes that end in /videos
// ------------------------------------------------------------------------------
router.route('/')

	.post(function(req, res) {
		
		var video = new Video();      // create a new instance of the video model
		video.name = req.body.name;  // set the videos name (comes from the request)
		video.channel = req.body.channel ? req.body.channel : null;

		video.save(function(err) {
			if (err)
				res.send(err);

			res.statusCode = 201;
			res.json(video);
		});
		
	})
	.put(function(req, res) {
		Video.findOneAndUpdate({
			name: req.body.name,
		},
		{
			channel: req.body.channel
		},
		// new: true returns the updated document instead of
		// the original one.
		{new: true},
		function(err, doc) {
			if(err) {
				res.sendStatus(500);
			}
			res.json(doc);
		});

	})
	.get(function(req, res) {
		if(req.query.channel) {
			Video.find({channel: req.query.channel}, function(err, videos) {
				if (err)
					res.send(err);

				res.json(videos);
			});
		}
		else {
			Video.find(function(err, videos) {
				if (err)
					res.send(err);

				res.json(videos);
			});
		}

	});


router.route('/:videoname')

    .delete(function(req, res) {
        Video.remove({
            name: req.params.videoname
        }, function(err, video) {
            if (err)
                res.send(err);

            res.sendStatus(204);
        });
    });

module.exports = router;