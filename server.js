// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var cookieSession = require('cookie-session');


mongoose.Promise = global.Promise; // Get rid of deprecation warning from Mongoose.
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cookieSession({
	name: 'hullabaloo',
	keys: ['key1', 'key2']
}));


var port = process.env.PORT || 8080;        // set our port

mongoose.connect('mongodb://tester:tester@ds139187.mlab.com:39187/sara-test'); // connect to our database


var User			= require('./app/models/user');
var UserRouter  	= require('./app/routes/users');
var AccountRouter 	= require('./app/routes/accounts');

var Video 			= require('./app/models/video');
var VideoRouter 	= require('./app/routes/videos');



// ROUTES FOR OUR API
// =============================================================================
var api_router = express.Router();              // get an instance of the express api_router
api_router.use('/accounts', AccountRouter);
api_router.use('/users', UserRouter);
api_router.use('/videos', VideoRouter);




// Very naive token based session authentication middleware.
app.use(function(req, res, next) {

	// If running in a test env we force the session to validate.
	// Pretend Saruman is logged in.
	if(process.env.TEST_ENV) {
		// Hack to disable forced authentication when running tests ...
		if(req.get('Nuke-Session')) {
			req.session = null;
		}
		else{
			req.session['username'] = "Saruman";
		}
	}

	if(req.originalUrl == "/api/accounts/auth" && req.method == "POST") {
		next();
	}
	else if(req.originalUrl == "/api/accounts/logout") {
		req.session = null;
		next();
	}
	else if(req.session && req.session['username']) {
		next();
	}
	else {
		res.sendStatus(401);	
	}

});


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
api_router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });   
});

/*
	NOTE! This is a very very hacky way of implementing a "mockableish" database, where we can
	drop it and populate it on the fly with unit tests.
 
	It saves us from doing a lot of fancy things to get a proper mock database up and running
	for our automated unit tests.
*/
api_router.get('/dropBears', function(req, res) {
	Bear.remove({}, function(err) {
		if(err) {
			res.statusCode = 501;
			res.send("Something went wrong.");
		}
		res.send('Bear collection nuked!');
	});
});

api_router.get('/dropAccounts', function(req, res) {
	User.remove({}, function(err) {
		if(err) {
			res.statusCode = 501;
			res.send("Something went wrong.");
		}
		res.send('Account/User collection nuked!');
	});
});

api_router.get('/dropVideos', function(req, res) {
	Video.remove({}, function(err) {
		if(err) {
			res.statusCode = 501;
			res.send("Something went wrong.");
		}
		res.send('Video collection nuked!');
	});
});

// REGISTER OUR ROUTES
// =============================================================================
// all of our routes will be prefixed with /api

app.use('/api', api_router);
// START THE SERVER

// =============================================================================
var server = app.listen(port, function() {
	var port = server.address().port;
	console.log("Our app is listening on port %s", port);
});


module.exports = server;