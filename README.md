# Quickstart
1. install NodeJS
2. run `npm install -g mocha`
3. run `npm install`
4. run `npm start` 

# Populating database
## On Windows:
1. run `npm run initdb_win64`
## On Unix:
1. run `npm run initdb_unix` 

# Testing
## On Windows:
1. run `npm run test_win64`
## On Unix:
2. run `npm run test_unix`
