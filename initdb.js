var request = require('supertest');
var assert = require('assert');

describe('Running database initialization script', function() {
	var server;
	beforeEach(function setup() {
		process.env['TEST_ENV'] = "true";
		server = require('./server');
	});
	afterEach(function teardown() {
		server.close();
	});

	it('Drop all Account from database', function(done) {
		request(server)
		.get('/api/dropAccounts')
		.expect(200, done);
	});

	it('Drop all Video information from database', function(done) {
		request(server)
		.get('/api/dropVideos')
		.expect(200, done);
	});

	it('Create Saruman user', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Saruman', password: 'YouShallPass'})
		.expect('{"message":"Account Saruman created with password YouShallPass!"}', done);
	});

	it('Can create Gandalf user', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf', password: "FlyYouFools"})
		.expect('{"message":"Account Gandalf created with password FlyYouFools!"}', done);
	});

	it('Create Gandalf The White', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf The White', password: "YouShallNotPass!"})
		.expect('{"message":"Account Gandalf The White created with password YouShallNotPass!!"}', done);
	});

	it('Create video CrazyCats', function(done) {
		request(server)
		.post('/api/videos')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats'})
		.expect(function(res) {
			assert.equal(res.body.name, "CrazyCats");
			assert.equal(res.body.channel, null);
		})
		.expect(201, done);
	});

	it('Can create video CrazyCats2 in channel Catz', function(done) {
		request(server)
		.post('/api/videos')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats2', channel: "Catz"})
		.expect(function(res) {
			assert.equal(res.body.name, "CrazyCats2");
			assert.equal(res.body.channel, "Catz");
		})
		.expect(201, done);
	});

	it('Can favorite video CrazyCats2', function(done) {
		request(server)
		.post('/api/users/favoriteVideo')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats2'})
		.expect(function(res) {
			assert.equal(res.body.favoriteVideos.length, 1);
			res.body.favoriteVideos.forEach(function(video) {
				assert.equal(video, "CrazyCats2");
			});
		})
		.expect(200, done);
	});

	it('Can close friend user Gandalf The White', function(done) {
		request(server)
		.post('/api/users/closeFriend')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf The White'})
		.expect(function(res) {
			assert.equal(res.body.closeFriends.length, 1);
			res.body.closeFriends.forEach(function(friend) {
				assert.equal(friend, "Gandalf The White");
			});
		})
		.expect(200, done);
	});
});
