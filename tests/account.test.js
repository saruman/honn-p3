var request = require('supertest');
var assert = require('assert');

describe('Running Account tests', function() {
	var server;
	beforeEach(function setup() {
		server = require('../server');
	});
	afterEach(function teardown() {
		server.close();
	});

	it('responds to /api/accounts', function(done) {
		request(server)
		.get('/api/accounts')
		.expect(200, done);
	});

	it('Contains 0 useraccounts', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 6 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 0);
		}).
		expect(200, done);
	});

	it('Can create Saruman user', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Saruman', password: 'YouShallPass'})
		.expect('{"message":"Account Saruman created with password YouShallPass!"}', done);
	});

	it('Contains 1 account', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 1 account in response.
		.expect(function(res) {
			assert.equal(res.body.length, 1);
		}).
		expect(200, done);
	});

	it('can log in as Saruman user', function(done) {
		request(server)
		.post('/api/accounts/auth')
		.set('Content-Type', 'application/json')
		.send({name: 'Saruman', password: 'YouShallPass'})
		.expect(200, done);
	});

	it('Cannot log in as Saruman with wrong password', function(done) {
		request(server)
		.post('/api/accounts/auth')
		.set('Content-Type', 'application/json')
		.send({name: 'Saruman', password: 'YouShallNotPass'})
		.expect(401, done);
	});

	it('Cannot get accounts if not authenticated', function(done) {
		request(server)
		.get('/api/accounts')
		.set('Nuke-Session', 'true')
		.expect(401, done);
	});

	it('Can create Gandalf user', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf', password: "FlyYouFools"})
		.expect('{"message":"Account Gandalf created with password FlyYouFools!"}', done);
	});

	it('Contains 2 users', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 2 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 2);
		}).
		expect(200, done);
	});

	it('Can change Gandalf password', function(done) {
		request(server)
		.post('/api/accounts/updatepassword')
		.set('Content-Type', 'application/json')
		.send({
			name: 'Gandalf', 
			oldPassword: 'FlyYouFools', 
			newPassword: 'AWizardIsNeverLate'
		})
		.expect(200, done);
	});

	it('Gandalf password is updated', function(done) {
		request(server)
		.get('/api/accounts/gandalf')
		.expect(function(res) {
			assert.equal(res.body.name, "Gandalf");
			assert.equal(res.body.password ,"AWizardIsNeverLate");
		}).
		expect(200, done);
	});

	it('Can create Gandalf The White user (name with spaces)', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf The White', password: "YouShallNotPass!"})
		.expect('{"message":"Account Gandalf The White created with password YouShallNotPass!!"}', done);
	});

	it('Contains 3 users', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 4 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 3);
		}).
		expect(200, done);
	});

	it('Cannot create Saruman user again (Already exists)', function(done) {
		request(server)
		.post('/api/accounts')
		.set('Content-Type', 'application/json')
		.send({name: 'Saruman', password: 'YouShallPass'})
		.expect(400, done);
	});

	it('Still contains 3 accounts', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 6 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 3);
		}).
		expect(200, done);
	});

	it('All users have name', function(done) {
		request(server)
		.get('/api/accounts')
		.expect(function(res) {
			res.body.forEach(function(accounts) {
				assert.ok(accounts.name);
			});
		}).
		expect(200, done);
	});

	it('All users have password', function(done) {
		request(server)
		.get('/api/accounts')
		.expect(function(res) {
			res.body.forEach(function(accounts) {
				assert.ok(accounts.password);
			});
		}).
		expect(200, done);
	});

	it('Delete Gandalf account', function(done) {
		request(server)
		.delete('/api/accounts/Gandalf')
		.expect(204, done);
	});

	it('Now contains 2 accounts', function(done) {
		request(server)
		.get('/api/accounts')
		// Check we have 2 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 2);
		}).
		expect(200, done);
	});

});
