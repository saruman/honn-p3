var request = require('supertest');
var assert = require('assert');

describe('Running Core tests (setup for rest of tests)', function() {
	var server;
	beforeEach(function setup() {
		server = require('../server');
	});
	afterEach(function teardown() {
		server.close();
	});

	it('Does not respond to /', function(done) {
		request(server)
		.get('/')
		.expect(404, done);
	});

	it('Responds to /api', function(done) {
		request(server)
		.get('/api')
		.expect(200, done);
	});

	it('Drop all Account from database', function(done) {
		request(server)
		.get('/api/dropAccounts')
		.expect(200, done);
	});

	it('Drop all Video information from database', function(done) {
		request(server)
		.get('/api/dropVideos')
		.expect(200, done);
	});

});