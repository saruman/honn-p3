var request = require('supertest');
var assert = require('assert');

describe('Running User tests', function() {
	var server;
	beforeEach(function setup() {
		server = require('../server');
	});
	afterEach(function teardown() {
		server.close();
	});

	it('responds to /api/users', function(done) {
		request(server)
		.get('/api/users')
		.expect(200, done);
	});

	it('Contains 2 users', function(done) {
		request(server)
		.get('/api/users')
		// Check we have 6 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 2);
		}).
		expect(200, done);
	});

	it('Can get current user profile', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.name, "Saruman");
			assert.equal(res.body.favoriteVideos.length, 0);
		})
		.expect(200, done);
	});

	it('Has username Saruman', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.name, "Saruman");
		})
		.expect(200, done);
	});

	it('Has 0 favorite videos', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.favoriteVideos.length, 0);
		})
		.expect(200, done);
	});

	it('Has 0 close friends', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.closeFriends.length, 0);
		})
		.expect(200, done);
	});

	it('Can favorite video CrazyCats2', function(done) {
		request(server)
		.post('/api/users/favoriteVideo')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats2'})
		.expect(function(res) {
			assert.equal(res.body.favoriteVideos.length, 1);
			res.body.favoriteVideos.forEach(function(video) {
				assert.equal(video, "CrazyCats2");
			});
		})
		.expect(200, done);
	});

	it('Can close friend user Gandalf The White', function(done) {
		request(server)
		.post('/api/users/closeFriend')
		.set('Content-Type', 'application/json')
		.send({name: 'Gandalf The White'})
		.expect(function(res) {
			assert.equal(res.body.closeFriends.length, 1);
			res.body.closeFriends.forEach(function(friend) {
				assert.equal(friend, "Gandalf The White");
			});
		})
		.expect(200, done);
	});

	it('Has 1 favorite videos', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.favoriteVideos.length, 1);
		})
		.expect(200, done);
	});

	it('Has 1 close friends', function(done) {
		request(server)
		.get('/api/users/current')
		.expect(function(res) {
			assert.equal(res.body.closeFriends.length, 1);
		})
		.expect(200, done);
	});

	it('Can update username to Saruman The Wise', function(done) {
		request(server)
		.put('/api/users/current')
		.set('Content-Type', 'application/json')
		.send({name: "Saruman The Wise"})
		.expect(function(res) {
			assert.equal(res.body.name, "Saruman The Wise");
		})
		.expect(200, done);
	});


});
