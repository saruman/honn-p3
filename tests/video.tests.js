var request = require('supertest');
var assert = require('assert');

describe('Running Video tests', function() {
	var server;
	beforeEach(function setup() {
		server = require('../server');
	});
	afterEach(function teardown() {
		server.close();
	});

	it('responds to /api/videos', function(done) {
		request(server)
		.get('/api/videos')
		.expect(200, done);
	});

	it('Contains 0 videos', function(done) {
		request(server)
		.get('/api/videos')
		// Check we have 6 accounts in response.
		.expect(function(res) {
			assert.equal(res.body.length, 0);
		}).
		expect(200, done);
	});

	it('Can create video CrazyCats', function(done) {
		request(server)
		.post('/api/videos')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats'})
		.expect(function(res) {
			assert.equal(res.body.name, "CrazyCats");
			assert.equal(res.body.channel, null);
		})
		.expect(201, done);
	});

	it('Contains 1 video', function(done) {
		request(server)
		.get('/api/videos')
		// Check we have 1 video in response.
		.expect(function(res) {
			assert.equal(res.body.length, 1);
		}).
		expect(200, done);
	});

	it('Can create video CrazyCats2 in channel Catz', function(done) {
		request(server)
		.post('/api/videos')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats2', channel: "Catz"})
		.expect(function(res) {
			assert.equal(res.body.name, "CrazyCats2");
			assert.equal(res.body.channel, "Catz");
		})
		.expect(201, done);
	});

	it('Contains 2 video', function(done) {
		request(server)
		.get('/api/videos')
		// Check we have 2 videos in response.
		.expect(function(res) {
			assert.equal(res.body.length, 2);
		}).
		expect(200, done);
	});


	it('Contains 1 video in channel Catz', function(done) {
		request(server)
		.get('/api/videos?channel=Catz')
		.expect(function(res) {
			assert.equal(res.body.length, 1);
			res.body.forEach(function(video) {
				assert.equal(video.channel, "Catz");
			});
		})
		.expect(200, done);
	});

	it('Can add already existing video CrazyCats to channel Catz', function(done) {
		request(server)
		.put('/api/videos/')
		.set('Content-Type', 'application/json')
		.send({name: 'CrazyCats', channel: 'Catz'})
		.expect(function(res) {
			assert.equal(res.body.name, 'CrazyCats');
			assert.equal(res.body.channel, 'Catz');
		})
		.expect(200, done);
	});


	it('Contains 2 video in channel Catz', function(done) {
		request(server)
		.get('/api/videos?channel=Catz')
		.expect(function(res) {
			assert.equal(res.body.length, 2);
			res.body.forEach(function(video) {
				assert.equal(video.channel, "Catz");
			});
		}
)		.expect(200, done);
	});

	it('All Videos have name', function(done) {
		request(server)
		.get('/api/videos')
		.expect(function(res) {
			assert.ok(res.body.length > 0);
			res.body.forEach(function(video) {
				assert.ok(video.name);
			});
		}).
		expect(200, done);
	});

	it('Can remove a video', function(done) {
		request(server)
		.delete('/api/videos/CrazyCats')
		.expect(204, done);
	});

	it('Contains 1 video in channel Catz', function(done) {
		request(server)
		.get('/api/videos?channel=Catz')
		.expect(function(res) {
			assert.equal(res.body.length, 1);
			res.body.forEach(function(video) {
				assert.equal(video.channel, "Catz");
			});
		})
		.expect(200, done);
	});

	it('Contains 1 video', function(done) {
		request(server)
		.get('/api/videos')
		.expect(function(res) {
			assert.equal(res.body.length, 1);
		})
		.expect(200, done);
	});

});
